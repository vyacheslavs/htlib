#ifndef __HTLIB_H
#define __HTLIB_H

#include <stdlib.h>
#include "log2.h"
#include <stddef.h>

// Упоминание о разработчиках
/*
 * Statically sized hash table implementation
 * (C) 2012  Sasha Levin <levinsasha928@gmail.com>
 */

struct hlist_node {
    struct hlist_node * next, ** pprev;
};

struct hlist_head {
    struct hlist_node * first;
};

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define DEFINE_HASHTABLE(name, bits)						\
	struct hlist_head name[1 << (bits)] =					\
			{ [0 ... ((1 << (bits)) - 1)] = HLIST_HEAD_INIT }

#define HLIST_HEAD_INIT { .first = NULL }
#define INIT_HLIST_HEAD(ptr) ((ptr)->first = NULL)

void __hash_init(struct hlist_head *ht, unsigned int sz);
void hlist_add_head(struct hlist_node *n, struct hlist_head *h);

/* Use hash_32 when possible to allow for fast 32bit hashing in 64bit kernels. */
#define hash_min(val, bits)							\
	(sizeof(val) <= 4 ? hash_32(val, bits) : hash_long(val, bits))

#define HASH_SIZE(name) (ARRAY_SIZE(name))
#define HASH_BITS(name) ilog2(HASH_SIZE(name))

#define hash_init(hashtable) __hash_init(hashtable, HASH_SIZE(hashtable))

#define hash_add(hashtable, node, key)						\
	hlist_add_head(node, &hashtable[hash_min(key, HASH_BITS(hashtable))])

void hash_del(struct hlist_node *node);

void INIT_HLIST_NODE(struct hlist_node *h);

uint32_t hash_32(uint32_t val, unsigned int bits);

#define GOLDEN_RATIO_PRIME_32 0x9e370001UL

#define container_of(ptr, type, member) ({                  \
    const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
    (type *)( (char *)__mptr - offsetof(type,member) );})

#define hlist_entry(ptr, type, member) container_of(ptr,type,member)

#define hlist_for_each_entry(pos, head, member)                         \
    for (pos = hlist_entry_safe((head)->first, typeof(*(pos)), member);\
    pos;                                                       \
    pos = hlist_entry_safe((pos)->member.next, typeof(*(pos)), member))

#define hlist_entry_safe(ptr, type, member) \
    ({ typeof(ptr) ____ptr = (ptr); \
    ____ptr ? hlist_entry(____ptr, type, member) : NULL; \
    })

#define hash_for_each_possible(name, obj, member, key) \
    hlist_for_each_entry(obj, &name[hash_min(key, HASH_BITS(name))], member)

#endif
