#include "htlib.h"
#include <stdio.h>

typedef struct {
    int somedata;
    struct hlist_node node;
} test_hashable_item;

#define cof(ptr, type, member) ({                  \
    const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
    (type *)( (char *)__mptr - offsetof(type,member) );})

int main() {
    DEFINE_HASHTABLE(ht, 8); // определяем 64битную хэш таблицу
    hash_init(ht);
    test_hashable_item t, t2, *it;
    t.somedata = 1;
    t2.somedata = 12;
    INIT_HLIST_NODE(&t.node);
    INIT_HLIST_NODE(&t2.node);
    hash_add(ht, &t.node, 1);
    hash_add(ht, &t2.node, 12);
    struct hlist_node * somenode;

    hash_for_each_possible( ht, it, node, 1 )
    {
        printf("val = %d\n", it->somedata);
    }

    return 0;
}
